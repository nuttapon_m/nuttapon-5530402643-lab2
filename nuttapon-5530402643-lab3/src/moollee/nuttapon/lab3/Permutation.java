package moollee.nuttapon.lab3;

public class Permutation {

	/**
	 * @param args
	 */
	public static void main(String[] args) {		
		// TODO Auto-generated method stub
		if (args.length !=2) {
			System.err.println("Usage:Permutation <n> <k> ");
			System.exit(1);
		}
		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		long result = permute(n,k);
		System.out.println("("+n+ "!/" + n + "-" + k + ")!= " +result );
	}

	private static long permute(int n, int k) {
		// TODO Auto-generated method stub
		System.out.println(n + "! = " + fact(n));
		System.out.println( "("+ n + " - " + k + ")! = " + fact(n-k) );
		return fact(n)/fact(n-k);
	}

	private static int fact(int n) {
		// TODO Auto-generated method stub
		int sum = 1;
		for(int i=1;i<=n;i++){
		sum *= i;	
		}
		return sum;
	}

}

