package moollee.nuttapon.lab3;

import java.util.Arrays;

public class SimpleStatsMethods {

		static int numGPAs;
		static double[] nums;
		static double sum ;
		public static void main(String[] args) {
			// TODO Auto-generated method stub
			if(args.length < 2){
				System.out.println("Usage:<SimpleStats> <numGPAs> <GPA>.." );
				System.exit(0);
			}
			numGPAs = Integer.parseInt(args[0]);
			nums = new double[numGPAs];
			acceptInput(args);
			displayStats(nums);
		}
		private static void displayStats(double[] nums2) {
			// TODO Auto-generated method stub
			for(int i=0;i<numGPAs;i++){
				sum += nums[i];
			}
		Arrays.sort(nums);
		
			System.out.println("\nStatus");		
		 	System.out.println("Avg GPA is "+ (sum/numGPAs));
		 	System.out.println("Min GPA is "+nums[0]);
		 	System.out.println("Max GPA is "+nums[numGPAs-1]);		
		}
		private static void acceptInput(String[] args) {
			// TODO Auto-generated method stub
			
				System.out.println("For the input GPAs : ");
			for (int i=1;i<=numGPAs;i++){
	                System.out.print(args[i]+" ");
	                nums[i-1] = Double.parseDouble(args[i]);
	          
			}
	
	}

}
