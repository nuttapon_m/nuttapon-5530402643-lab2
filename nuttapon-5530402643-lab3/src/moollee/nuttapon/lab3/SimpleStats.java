package moollee.nuttapon.lab3;

import java.util.Arrays;

public class SimpleStats {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int i,num;
		double sum = 0;
		num = Integer.parseInt(args[0]);
		double gpas[] = new double[num];
		
		System.out.println("For the input GPAs: ");
		for(i=0 ; i <= num-1 ; i++){
			gpas[i] = Double.parseDouble(args[i]);
			System.out.print(gpas[i] + " ");
			sum = sum + gpas[i];
		}
		System.out.println();
		Arrays.sort(gpas);
		
		System.out.println("Stats:");
		System.out.println("Avg GPA is " + sum/num);
		System.out.println("Min GPA is " + gpas[0]);
		System.out.println("Max GPA is " + gpas[num-1]);
	}

}
